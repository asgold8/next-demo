# Thank you, Next.js

### Try it out here: [Next.js Demo](https://next-demo.asgold8.now.sh/)

This demo is the culmination of following the tutorials provided on the Next.js
website. 

[Next.js Learn](https://nextjs.org/learn/basics/getting-started)

