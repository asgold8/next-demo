import Link from "next/link";

const Header = () => {
  return (
    <header>
        <Link href="/">
          <a>Home</a>
        </Link>      
        <Link href="/about">
          <a>About</a>
        </Link>
        <Link href="/blog">
          <a>Fake Blog</a>
        </Link>
        <Link href="/quoteFetcher">
          <a>Quote Generator</a>
        </Link>
      <style jsx>
        {`
          a {            
            color: white;
            margin-right: 15px;
          }

          a:hover {
            opacity: 0.6;
          }
        `}
      </style>
    </header>
  );
};

export default Header;
