import Layout from '../components/MyLayout';

export default function About() {
  return (
    <Layout>      
      <h1>About the project</h1>
      <div>
        <p>Welcome to the about page. This is a small demo using 
          Next.js with React.js following the lessons here:</p>
          <a href="https://nextjs.org/learn/basics/getting-started">Learn Next.js</a>
      </div>
    </Layout>
  )
}