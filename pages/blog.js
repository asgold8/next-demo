import Link from "next/link";

import Layout from "../components/MyLayout";

function getPosts() {
  return [
    { id: "hello-nextjs", title: "Hello Next.js" },
    { id: "learn-nextjs", title: "Learn Next.js" },
    { id: "deploy-nextjs", title: "Deploy apps with ZEIT" }
  ];
}

const PostLink = ({post}) => {
 return(
  <li>
    <Link href="/b/[id]" as={`/b/${post.id}`}>
      <a>{post.title}</a>
    </Link>
  </li>);
};

const Blog = () => {
  return (
    <Layout>
      <h1>My Fake Blog</h1>
      <span>Similar to the homepage, these links demonstrate creating
        dynamic pages and routes from json objects.
      </span>
      <ul>
        {getPosts().map(post => (
          <PostLink key={post.id} post={post}/>
        ))}
      </ul>
      <style jsx>
        {`
          h1,
          a {
            font-family: "Arial";
          }
          ul {
            padding: 0;
          }
          li {
            list-style: none;
            margin: 5px 0;
          }
          a {
            text-decoration: none;
            color: darkgray;
          }
          a:hover {
            opacity: 0.6;
          }
        `}
      </style>
    </Layout>
  );
};

export default Blog;


