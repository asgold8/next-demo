import Link from "next/link";
import fetch from "isomorphic-unfetch";

import Layout from "../components/MyLayout";

const Index = props => (
  <Layout>
    <h1>Nice little demo of Next.js</h1>
    <span>The list below is a demonstration from the </span>
      <a href="https://nextjs.org/learn/basics/getting-started">Learn Next.js</a> 
      <span> wherein by querying api.tvmaze.com, a list of links and dynamic pages 
      are returned based on the results from the api. Try clicking on any of the links below.
    </span>
    <h3>Star Trek shows:</h3>
    <ul>
      {props.shows.map(show =>(
        <li key={show.id}>
          <Link href="/p/[id]" as={`/p/${show.id}`}>
            <a>{show.name}</a>
          </Link>
        </li>
      ))}
    </ul>
  </Layout>
);

Index.getInitialProps = async function() {
  const res = await fetch('https://api.tvmaze.com/search/shows?q=star-trek');
  const data = await res.json();

  console.log(`Show data fetched. Count: ${data.length}`);

  return {
    shows: data.map(entry => entry.show)
  };
};

export default Index;
